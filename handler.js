const AWS = require('aws-sdk');

const axios = require('axios');
const apiUrl = 'https://slack.com/api';

const homeView = require('./views/home');

const dynamoDb = new AWS.DynamoDB.DocumentClient({
  api_version: '2012-08-10',
  region: 'us-west-1'
});

exports.handler = async (request) => {
  const body = JSON.parse(request.body);
  const { event, type: requestType } = body;

  console.log(request);

  if (requestType === 'url_verification') {
    return {
      statusCode: 200,
      body: JSON.stringify({ challenge: event.body }),
    };
  }

  const { type: eventType, user } = event;

  if (requestType === 'event_callback' && eventType === 'app_home_opened') {
    const data = {
      user_id: user,
      view: homeView
    };

    const params = {
      TableName: 'slackUsers',
      Key: {
        'id' : user,
      }
    };

    const record = await dynamoDb.get(params).promise();

    if (!record) {
      return { statusCode: 400, };
    }

    const result = await axios.post(`${apiUrl}/views.publish`, data, { headers: { Authorization: `Bearer ${record.Item.authToken}` } });

    if(result.data.error) {
      return { statusCode: 400, };
    }

    return { statusCode: 200 };
  }

  return { statusCode: 404 };
};
