module.exports = {
  type: 'home',
  blocks: [
    {
      type: 'section',
      text: {
          type: 'mrkdwn',
          text: '*Welcome, Guys!* \nThis is a test task for Clerk project by Eugene Safronov!',
      },
      accessory: {
        type: 'button',
        style: 'primary',
        action_id: 'add_record',
        text: {
          type: 'plain_text',
          text: 'Add new record',
          emoji: true,
        },
      },
    },
    {
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: ':wave: Hey, my source code is on gitlab <https://gitlab.com/testslackapp1>!',
        },
      ],
    },
    {
      type: 'divider',
    },
  ],
};
